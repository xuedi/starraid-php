<?php

namespace Xuedi\Starraid\Client;

use Exception;
use React\Socket\ConnectionInterface;
use React\Socket\Connector;
use React\Stream\WritableResourceStream;
use Xuedi\Starraid\Common\Messages\Login;
use Xuedi\Starraid\Common\Messages\Ping;

class App
{
    public function init(string $user, string $pass)
    {
        $connector = new Connector();
        $connector->connect('127.0.0.1:3000')->then(function (ConnectionInterface $connection) use ($user, $pass) {
            //$connection->pipe(new WritableResourceStream(STDOUT));


            $loginMsg = new Login($user, $pass);


            $connection->write($loginMsg->jsonSerialize());
        }, function (Exception $e) {
            echo 'Error: ' . $e->getMessage() . PHP_EOL;
        });
    }

    public function run()
    {
        echo "client::run" . PHP_EOL;
    }
}
