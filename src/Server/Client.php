<?php

namespace Xuedi\Starraid\Server;

class Client
{
    private string $connection;

    public function __construct(string $connection)
    {
        $this->connection = $connection;
    }
}
