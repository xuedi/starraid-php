<?php

namespace Xuedi\Starraid\Server;

use React\EventLoop\Loop;
use React\Socket\ConnectionInterface;
use React\Socket\SocketServer;

class App
{
    private Status $status;
    private Network $network;

    // TODO: do autowiring
    public function __construct()
    {
        $this->network = new Network();
        $this->status = new Status(
            $this->network
        );
    }

    public function init()
    {
        $this->status->service_start('events');
        $this->registerEvents();
        $this->status->service_ok();

        $this->status->service_start('network');
        $this->network->registerNetwork();
        $this->status->service_ok();

        $this->status->message("---------------------");

    }

    public function run()
    {
        Loop::run();
    }

    private function registerEvents(): void
    {
        // classic tick actions
        Loop::addPeriodicTimer(1, function () {
            $this->status->tick();
        });

        // register the end of app
        Loop::addSignal(SIGINT, $func = function ($signal) use (&$func) {
            exit;
        });

        // status update
        Loop::addPeriodicTimer(10, function () {
            $this->status->info();
        });
    }
}
