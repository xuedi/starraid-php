<?php

namespace Xuedi\Starraid\Server;

use React\Socket\ConnectionInterface;
use React\Socket\SocketServer;
use Throwable;

class Network
{
    private array $clients;

    public function __construct()
    {
        $this->clients = [];
    }

    public function add(string $connection)
    {
        echo '[' . $connection . ' connected]' . PHP_EOL;
        $this->clients[$connection] = new Client($connection);
    }

    public function getNumber(): int
    {
        return count($this->clients);
    }

    public function registerNetwork()
    {
        $socket = new SocketServer('127.0.0.1:3000'); //TODO: use tls and so on

        $socket->on('connection', function (ConnectionInterface $connection) {
            $this->add($connection->getRemoteAddress());

            //$connection->pipe(new WritableResourceStream(STDOUT));

            $connection->once('data', function ($chunk) use ($connection) {
                try {
                    //if(json_validate($chunk)) { // TODO: php8.3+
                    $data = json_decode($chunk, true);
                    $msg = $data['message']::fromData($data['data']);
                    dump($msg);
                    //}
                    //$connection->write((new Pong())->jsonSerialize());
                } catch (Throwable $exception) {
                    dump($exception);
                }
            });

            $connection->on('close', function () use ($connection) {
                echo '[' . $connection->getRemoteAddress() . ' disconnected]' . PHP_EOL;
            });
        });
    }
}
