<?php

namespace Xuedi\Starraid\Server;

class Status
{
    private int $ticks;
    private Network $clients;

    public function __construct(Network $clients)
    {
        $this->ticks = 0;
        $this->clients = $clients;
    }

    public function tick()
    {
        $this->ticks++;
    }

    public function info()
    {
        $formatted = sprintf('Mem: %sKiB, Clients: %s, Ticks: %d',
            number_format(memory_get_usage() / 1024),
            $this->clients->getNumber(),
            $this->ticks
        );
        //dump($this->clients);

        $this->ticks = 0;
        $this->message($formatted);
    }

    public function message(string $message, bool $eol = true, bool $showTicks = true)
    {
        if ($showTicks) {
            echo sprintf("%s: ", date('Y-m-d H:i:s'));
        }
        echo $message;
        if ($eol) {
            echo PHP_EOL;
        }
    }

    public function service_start(string $name)
    {
        $this->message("$name ... ", false);
    }

    public function service_ok()
    {
        $this->message('OK', true, false);
    }

    public function service_failed(string $error)
    {
        $this->message('FAILED', true, false);
        $this->message($error);
        die(1);
    }
}
