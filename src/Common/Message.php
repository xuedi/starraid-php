<?php

namespace Xuedi\Starraid\Common;

use JsonSerializable;

abstract class Message implements JsonSerializable, MessageInterface
{
    public function getType(): string
    {
        return get_class($this);
    }

    public function jsonSerialize(): string
    {
        return json_encode([
            'message' => $this->getType(),
            'data' => $this->getData(),
        ], JSON_THROW_ON_ERROR | true);
    }
}
