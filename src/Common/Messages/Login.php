<?php

namespace Xuedi\Starraid\Common\Messages;

use Xuedi\Starraid\Common\Message;

class Login extends Message
{
    private string $name;
    private string $pass;

    public static function fromData(array $data): self
    {
        // validate
        return new self($data['name'],  $data['pass']);
    }

    public function __construct(string $name, string $pass)
    {
        $this->name = $name;
        $this->pass = $pass;
    }

    public function getData(): array
    {
        return [
            'name' => $this->name,
            'pass' => $this->pass,
        ];
    }

    public function getPass(): string
    {
        return $this->pass;
    }

    public function getName(): string
    {
        return $this->name;
    }
}
