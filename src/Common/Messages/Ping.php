<?php

namespace Xuedi\Starraid\Common\Messages;

use Xuedi\Starraid\Common\Message;

class Ping extends Message
{
    public function getData(): array
    {
        return [];
    }

    public static function fromData(array $data): self
    {
        // TODO: Implement rebuild() method.
    }
}
