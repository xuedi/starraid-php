<?php

namespace Xuedi\Starraid\Common;

interface MessageInterface
{
    public function getType(): string;

    public function getData(): array;

    public static function fromData(array $data): self;
}
