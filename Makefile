
default: help


help: ## Show this help
	@cat $(MAKEFILE_LIST) | grep -e "^[a-zA-Z_\-]*: *.*## *" | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}'

install: ## Runs all kind of stuff
	composer install

update: ## Runs all kind of stuff
	composer update
	composer dump-autoload

run_server: ## run server only (foreground)
	@./bin/server

run_clientA: ## run client only (foreground)
	@./bin/client xuedi 12345

run_clientB: ## run client only (foreground)
	@./bin/client user1 10050

run_clientC: ## run client only (foreground)
	@./bin/client user2 20075
